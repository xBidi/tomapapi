package com.roman.cafe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class CustomExceptionHandler {

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  private void duplicatedUsernameException(
      final HttpServletRequest request, final HttpServletResponse response, Exception ex)
      throws IOException {
    ex.printStackTrace();
    response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getCause().getMessage());
  }
}
