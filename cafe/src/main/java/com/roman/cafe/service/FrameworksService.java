package com.roman.cafe.service;

import com.roman.cafe.model.Framework;
import com.roman.cafe.repositories.FrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FrameworksService {

  @Autowired public FrameworkRepository frameworkRepository;

  @Transactional(rollbackFor = Exception.class, readOnly = true)
  public List<Framework> findAll() {
    return frameworkRepository.findAll();
  }

  @Transactional(rollbackFor = Exception.class)
  public Framework create(Framework framework) {
    return frameworkRepository.save(framework);
  }

  @Transactional(rollbackFor = Exception.class)
  public void delete(Integer frameworkId) {
    frameworkRepository.deleteById(frameworkId);
  }
}
