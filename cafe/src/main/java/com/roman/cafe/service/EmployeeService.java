package com.roman.cafe.service;

import com.roman.cafe.model.Employee;
import com.roman.cafe.model.Framework;
import com.roman.cafe.repositories.EmployeeRepository;
import com.roman.cafe.repositories.FrameworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

  @Autowired public EmployeeRepository employeeRepository;
  @Autowired public FrameworkRepository frameworkRepository;

  @Transactional(rollbackFor = Exception.class, readOnly = true)
  public List<Employee> findAll() {
    return employeeRepository.findAll();
  }

  @Transactional(rollbackFor = Exception.class, readOnly = true)
  public Employee login(String email, String password) throws Exception {
    Optional<Employee> optionalEmployee = employeeRepository.findByEmail(email);
    if (!optionalEmployee.isPresent()) {
      throw new Exception("invalid email");
    }
    Employee employee = optionalEmployee.get();
    if (!employee.doPasswordsMatch(password)) {
      throw new Exception("invalid password");
    }
    return employee;
  }

  @Transactional(rollbackFor = Exception.class)
  public Employee addFramework(Integer employeeId, Integer frameworkId) {
    Employee employee = employeeRepository.findById(employeeId).get();
    Framework framework = frameworkRepository.findById(frameworkId).get();
    employee.getFrameworks().add(framework);
    return employeeRepository.save(employee);
  }

  @Transactional(rollbackFor = Exception.class)
  public Employee removeFramework(Integer employeeId, Integer frameworkId) {
    Employee employee = employeeRepository.findById(employeeId).get();
    Framework framework = frameworkRepository.findById(frameworkId).get();
    employee.getFrameworks().remove(framework);
    return employeeRepository.save(employee);
  }

  @Transactional(rollbackFor = Exception.class)
  public Employee create(Employee employee) {
    return employeeRepository.save(employee);
  }
}
