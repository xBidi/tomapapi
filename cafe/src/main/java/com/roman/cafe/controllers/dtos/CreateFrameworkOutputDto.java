package com.roman.cafe.controllers.dtos;

import com.roman.cafe.model.Framework;
import com.roman.cafe.model.enums.CategoryEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class CreateFrameworkOutputDto {

  private Integer id;
  private String name;
  private String description;
  private CategoryEnum category;
  private String image;

  public CreateFrameworkOutputDto(Framework framework) {
    this.id = framework.getId();
    this.name = framework.getName();
    this.description = framework.getDescription();
    this.category = framework.getCategory();
    this.image = framework.getImage();
  }
}
