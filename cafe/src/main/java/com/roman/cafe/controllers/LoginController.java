package com.roman.cafe.controllers;

import com.roman.cafe.model.Employee;
import com.roman.cafe.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v0/login")
public class LoginController {

  @Autowired public EmployeeService employeeService;

  @PostMapping
  public LoginResponse login(@RequestBody LoginInputDto loginInputDto) throws Exception {
    String email = loginInputDto.getEmail();
    String password = loginInputDto.getPassword();
    Employee employee = employeeService.login(email, password);
    return new LoginResponse(employee);
  }
}
