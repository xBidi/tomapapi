package com.roman.cafe.controllers.dtos;

import com.roman.cafe.model.enums.CategoryEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class CreateFrameworkInputDto {

  private String name;
  private String description;
  private CategoryEnum category;
  private String image;
}
