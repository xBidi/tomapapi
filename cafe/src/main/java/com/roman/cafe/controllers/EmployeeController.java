package com.roman.cafe.controllers;

import com.roman.cafe.controllers.dtos.EmployeeDto;
import com.roman.cafe.controllers.dtos.GetAllEmployeesDto;
import com.roman.cafe.controllers.dtos.RegisterEmployeeDto;
import com.roman.cafe.controllers.dtos.RegisterEmployeeResponseDto;
import com.roman.cafe.model.Employee;
import com.roman.cafe.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v0/employees")
public class EmployeeController {
  @Autowired public EmployeeService employeeService;

  @GetMapping
  public List<GetAllEmployeesDto> getAll() {
    List<Employee> employees = employeeService.findAll();
    return employees.stream().map(GetAllEmployeesDto::new).collect(Collectors.toList());
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public RegisterEmployeeResponseDto create(@RequestBody RegisterEmployeeDto registerEmployeeDto) {
    String email = registerEmployeeDto.getEmail();
    String name = registerEmployeeDto.getName();
    String password = registerEmployeeDto.getPassword();
    String surname = registerEmployeeDto.getSurname();
    String phoneNumber = registerEmployeeDto.getPhoneNumber();

    Employee employee = new Employee(name, surname, phoneNumber, email, password);
    employee.setLvl(registerEmployeeDto.getLvl());
    Employee createdEmployee = employeeService.create(employee);
    return new RegisterEmployeeResponseDto(createdEmployee);
  }

  @PutMapping("/{employeeId}/frameworks/{frameworkId}")
  public EmployeeDto addFramework(
      @PathVariable Integer employeeId, @PathVariable Integer frameworkId) {
    Employee employees = employeeService.addFramework(employeeId, frameworkId);
    return new EmployeeDto(employees);
  }

  @DeleteMapping("/{employeeId}/frameworks/{frameworkId}")
  public EmployeeDto removeFramework(
      @PathVariable Integer employeeId, @PathVariable Integer frameworkId) {
    Employee employees = employeeService.removeFramework(employeeId, frameworkId);
    return new EmployeeDto(employees);
  }
}
