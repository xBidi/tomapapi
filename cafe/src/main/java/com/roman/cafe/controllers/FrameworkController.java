package com.roman.cafe.controllers;

import com.roman.cafe.controllers.dtos.CreateFrameworkInputDto;
import com.roman.cafe.controllers.dtos.CreateFrameworkOutputDto;
import com.roman.cafe.controllers.dtos.GetAllFrameworksDto;
import com.roman.cafe.model.Framework;
import com.roman.cafe.model.enums.CategoryEnum;
import com.roman.cafe.service.FrameworksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v0/frameworks")
public class FrameworkController {
  @Autowired public FrameworksService frameworksService;

  @GetMapping
  public List<GetAllFrameworksDto> getAll() {
    List<Framework> frameworks = frameworksService.findAll();
    return frameworks.stream().map(GetAllFrameworksDto::new).collect(Collectors.toList());
  }

  @PostMapping
  public CreateFrameworkOutputDto create(
      @RequestBody CreateFrameworkInputDto createFrameworkInputDto) {
    CategoryEnum categoryEnum = createFrameworkInputDto.getCategory();
    String description = createFrameworkInputDto.getDescription();
    String image = createFrameworkInputDto.getImage();
    String name = createFrameworkInputDto.getName();
    Framework framework = new Framework(null, name, description, categoryEnum, image);
    Framework createdFramework = frameworksService.create(framework);
    return new CreateFrameworkOutputDto(createdFramework);
  }

  @ResponseStatus(HttpStatus.NO_CONTENT)
  @DeleteMapping("{frameworkId}")
  public void delete(@PathVariable Integer frameworkId) {
    frameworksService.delete(frameworkId);
  }
}
