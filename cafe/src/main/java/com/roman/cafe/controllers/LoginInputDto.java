package com.roman.cafe.controllers;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class LoginInputDto {
  private String email;
  private String password;
}
