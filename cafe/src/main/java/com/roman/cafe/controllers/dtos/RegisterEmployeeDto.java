package com.roman.cafe.controllers.dtos;

import com.roman.cafe.model.enums.LevelsEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class RegisterEmployeeDto {

  private String name;
  private String surname;
  private String phoneNumber;
  private String email;
  private String password;
  private LevelsEnum lvl;
}
