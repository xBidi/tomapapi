package com.roman.cafe.controllers;

import com.roman.cafe.model.Employee;
import com.roman.cafe.model.Framework;
import com.roman.cafe.model.enums.LevelsEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
public class LoginResponse {
  private Integer id;
  private String name;
  private String surname;
  private String phoneNumber;
  private String email;
  private LevelsEnum lvl = LevelsEnum.USER;
  private List<Framework> frameworks = new ArrayList<>();

  public LoginResponse(Employee employee) {
    this.id = employee.getId();
    this.name = employee.getName();
    this.surname = employee.getSurname();
    this.phoneNumber = employee.getPhoneNumber();
    this.email = employee.getEmail();
    this.lvl = employee.getLvl();
    this.frameworks = employee.getFrameworks();
  }
}
