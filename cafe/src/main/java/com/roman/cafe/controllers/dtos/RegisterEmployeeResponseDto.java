package com.roman.cafe.controllers.dtos;

import com.roman.cafe.model.Employee;
import com.roman.cafe.model.Framework;
import com.roman.cafe.model.enums.LevelsEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Getter
public class RegisterEmployeeResponseDto {
  private List<Framework> frameworks;
  private String email;
  private Integer id;
  private LevelsEnum lvl;
  private String name;
  private String phoneNumber;
  private String surname;

  public RegisterEmployeeResponseDto(Employee createdEmployee) {
    this.frameworks = createdEmployee.getFrameworks();
    this.email = createdEmployee.getEmail();
    this.id = createdEmployee.getId();
    this.lvl = createdEmployee.getLvl();
    this.name = createdEmployee.getName();
    this.phoneNumber = createdEmployee.getPhoneNumber();
    this.surname = createdEmployee.getSurname();
  }
}
