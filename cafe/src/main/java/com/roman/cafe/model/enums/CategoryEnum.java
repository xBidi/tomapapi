package com.roman.cafe.model.enums;

public enum CategoryEnum {
    DEVOPS,
    BACKEND,
    FRONTEND,
    FUNCTIONALITY
}
