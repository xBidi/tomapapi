package com.roman.cafe.model;

import com.roman.cafe.model.enums.CategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Framework {

  @Id @GeneratedValue private Integer id;

  @Column(unique = true)
  private String name;

  private String description;

  @Enumerated(value = EnumType.STRING)
  private CategoryEnum category;

  @Lob private String image;
}
