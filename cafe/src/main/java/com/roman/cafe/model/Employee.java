package com.roman.cafe.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.roman.cafe.model.enums.LevelsEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Entity
@NoArgsConstructor
public class Employee {
  @Id @GeneratedValue private Integer id;

  private String name;
  private String surname;
  private String phoneNumber;

  @Column(unique = true)
  private String email;

  private String password;

  @Enumerated(value = EnumType.STRING)
  @Setter
  private LevelsEnum lvl = LevelsEnum.USER;

  @OneToMany(
      cascade = {
        CascadeType.DETACH,
        CascadeType.MERGE,
        CascadeType.PERSIST,
        CascadeType.REFRESH,
        CascadeType.REMOVE
      },
      fetch = FetchType.EAGER)
  @JsonManagedReference
  private List<Framework> frameworks = new ArrayList<>();

  public Employee(String name, String surname, String phoneNumber, String email, String password) {
    this.name = name;
    this.surname = surname;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.setPassword(password);
  }

  public boolean doPasswordsMatch(final String password) {
    final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    return encoder.matches(password, this.password);
  }

  public void updatePassword(final String password) {
    if (StringUtils.isNotBlank(password)) {
      this.setPassword(password);
    }
  }

  private void setPassword(final String password) {
    this.password = password;
    this.hash();
  }

  private void hash() {
    String regex = "^\\$2[ayb]\\$.{56}$";
    Pattern p = Pattern.compile(regex);
    Matcher m = p.matcher(this.getPassword());
    if (!m.matches()) {
      this.setPassword(hash(this.password));
    }
  }

  private String hash(String p) {
    BCryptPasswordEncoder coder = new BCryptPasswordEncoder(6);
    return coder.encode(p);
  }
}
