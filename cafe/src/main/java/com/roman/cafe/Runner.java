package com.roman.cafe;

import com.roman.cafe.model.Employee;
import com.roman.cafe.repositories.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class Runner implements ApplicationRunner {

  EmployeeRepository employeeRepository;
  @Override
  public void run(ApplicationArguments args) {
    Employee employee = employeeRepository.findById(24).get();
    employee.updatePassword("password");
    employeeRepository.save(employee);
  }
}
